package com.korita.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.korita.bean.UserBean;
import com.korita.exception.ErrorStrings;
import com.korita.exception.KoritaException;
import com.korita.services.UserServices;

/**
 * @author Ashok 
 * This is a UserController class , which will be used for Signing
 *         up user, Login user , reset password
 */
@RestController
public class UserController {
	private static final Logger LOGGER = LogManager.getLogger(UserController.class.getName());
	@Autowired
	UserServices userServices;
	
	/**
	 * Accept the user request for signing up in application 
	 * @param userInput
	 * @return a Json String
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST, headers = "Accept=application/json")
	public String signup(@RequestBody UserBean userInput) {
		try {
			LOGGER.info("A new request for signup application using email : "+userInput.getEmail());
			boolean signUpFieldValidate = userInput.signUpFieldValidate();
			if (signUpFieldValidate) {
			LOGGER.debug("User has entered all the fields for signup");
				String response = userServices.signUpOperation(userInput);
				return response;
			} else {
				LOGGER.error("User has not entered all the fields for the signup process");
				throw new KoritaException(ErrorStrings.incompleteForm);
			}
		} catch (KoritaException e) {
			LOGGER.error("There is Korita Error during signup process in UserController", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getLocalizedMessage() + "\"}";
			return error;
		} catch (Exception e) {
			LOGGER.error("There is some exception during signup process in UserController", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + ErrorStrings.errorTech + "\"}";
			return error;
		}

	}
	

	/**
	 * Accept the user email and password to validate during login process
	 * @param userInput
	 * @return a Json String
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
	public String login(@RequestBody UserBean userInput) {
		try {
			LOGGER.info("There is login request for email : "+userInput.getEmail());
			boolean loginFieldValidation = userInput.loginFieldValidate();
			if (loginFieldValidation) {
				LOGGER.debug("User has entered all the fields for login process");
				String response = userServices.loginOperation(userInput);
				return response;
			} else {
				LOGGER.error("User has not entered all the fields for the login process");
				throw new KoritaException(ErrorStrings.incompleteForm);
			}
		} catch (KoritaException e) {
			LOGGER.error("There is Korita Error during login process in UserController", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getLocalizedMessage() + "\"}";
			return error;
		} catch (Exception e) {
			LOGGER.error("There is some exception during login process in UserController", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + ErrorStrings.errorTech + "\"}";
			return error;
		}

	}

}

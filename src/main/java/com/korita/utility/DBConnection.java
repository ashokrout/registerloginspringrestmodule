package com.korita.utility;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DBConnection {
	private static final Logger LOGGER = LogManager.getLogger(DBConnection.class.getName());

	/**
	 * Create HSQLdatabase connection object for JUnti Testing
	 * 
	 * @return Connection
	 */
	public static Connection getHSQLConnection() {
		Connection con = null;
		LOGGER.debug("Connection object will be created for local HSQL database for JUnit Test");
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection("jdbc:hsqldb:mem:employees", "korita", "korita");
		} catch (Exception e) {
			LOGGER.debug("There is some exception while creating HSQLconnection object", e);
		}
		return con;
	}
}

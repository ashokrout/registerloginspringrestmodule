package com.korita.bean;

import com.korita.exception.ErrorStrings;
import com.korita.exception.KoritaException;

/**
 * @author Ashok This is a bean class having user details fields
 */
public class UserBean {

	/**
	 * The first last name of this user.
	 */
	String firstName;
	/**
	 * The email address of this user.
	 */
	String email;
	/**
	 * The password of this user.
	 */
	String password;
	/**
	 * The hashedpassword which is created using password hashing utility of this user for password
	 */
	String hashedPassword;

	public UserBean() {
		super();
	}

	public UserBean(String email, String hashedPassword) {
		super();
		this.email = email;
		this.hashedPassword = hashedPassword;
	}

	/**
	 * Validate the user input for signup process, 
	 * @return boolean
	 * @throws KoritaException
	 */
	public boolean signUpFieldValidate() throws KoritaException {
		if (this.firstName == null || this.firstName.isEmpty()) {
			throw new KoritaException(ErrorStrings.firstNameMissing);
		}
		if (this.email == null || this.email.isEmpty()) {
			throw new KoritaException(ErrorStrings.emailMissing);
		}
		if (this.password == null || this.password.isEmpty()) {
			throw new KoritaException(ErrorStrings.passwordMissing);
		}
		return true;
	}

	/**
	 * Validate the user input for login process, 
	 * @return boolean
	 * @throws KoritaException
	 */
	public boolean loginFieldValidate() throws KoritaException {
		if (this.email == null || this.email.isEmpty()) {
			throw new KoritaException(ErrorStrings.emailMissing);
		}
		if (this.password == null || this.password.isEmpty()) {
			throw new KoritaException(ErrorStrings.passwordMissing);
		}
		return true;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

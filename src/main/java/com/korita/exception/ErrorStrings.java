package com.korita.exception;

public class ErrorStrings {
	public static final String errorTech = "There is technical Error";
	
	public static final String incompleteForm = "All mandatory fields are not entered in the form";
	
	public static final String firstNameMissing = "First Name is not entered";
	
	public static final String emailMissing = "Email is not entered";
	
	public static final String passwordMissing = "Password is not entered";
	
	public static final String alreadyRegistered = "You have already registered";
	
	public static final String invalidUserNamePassword  = "Invalid Email password combination";

}

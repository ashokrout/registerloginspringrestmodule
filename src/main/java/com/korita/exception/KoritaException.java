package com.korita.exception;

public class KoritaException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7901359981967825920L;

	public KoritaException(String errorMessage) {
		super(errorMessage);
	}

}

package com.korita.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.korita.bean.UserBean;
import com.korita.dao.UserDAO;
import com.korita.exception.ErrorStrings;
import com.korita.exception.KoritaException;
import com.korita.utility.PasswordHashing;

@Service("userServices")
public class UserServices {
	private static final Logger LOGGER = LogManager.getLogger(UserServices.class.getName());
	
	@Autowired
	UserDAO userDAO;

	/**
	 * Covert the user password to a hashing password and then send user input details to DAO layer for database operation
	 * @param userInput
	 * @return a Json String
	 */
	public String signUpOperation(UserBean userInput) {
		try {
			LOGGER.info("Checking if email " +userInput.getEmail() +" is already registered or not before signup");
			UserBean userAvailableCheck = userDAO.findUserByEmail(userInput);
			if (userAvailableCheck.getEmail()==null) {
				LOGGER.debug("User password will be converted now to secure hashedpassword before storing same in database");
				String hashedPassword = PasswordHashing.generateStorngPasswordHash(userInput.getPassword());
				userInput.setHashedPassword(hashedPassword);
				LOGGER.debug("password is hashed and now DAO layed will be called to create new user in database");
				int userCreated = userDAO.signUpUser(userInput);
				if (userCreated==1) {
					String finalResponse = "{\"Result\":\"OK\"}";
					return finalResponse;
				}else {
					String error = "{\"Result\":\"ERROR\",\"Message\":\"" + ErrorStrings.errorTech + "\"}";
					return error;
				}
			}else {
				LOGGER.debug("Email"+userInput.getEmail()+" is already a registered user in application");
				String error = "{\"Result\":\"ERROR\",\"Message\":\"" + ErrorStrings.alreadyRegistered + "\"}";
				return error;

			}
		} catch (KoritaException e) {
			LOGGER.error("There is Korita Error during signup process in UserService", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getLocalizedMessage() + "\"}";
			return error;
		} catch (Exception e) {
			LOGGER.error("There is some exception during signup process in UserService", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + ErrorStrings.errorTech + "\"}";
			return error;
		}
	}
	
	/**
	 * @param userInput
	 * @return a Json String
	 */
	public String loginOperation(UserBean userInput) {

		try {
			LOGGER.info("User email check will be done by calling DAO layer");
			UserBean userDetailsByEmail = userDAO.findUserByEmail(userInput);
			if (userDetailsByEmail.getEmail()==null) {
				LOGGER.error("Email"+userInput.getEmail()+" is not a registered user in application");
				throw new KoritaException("User is not registered with us Please sign up first");
			} else {
				LOGGER.debug("User provided password will be validated against the password user has created");
				boolean verifyPassword = PasswordHashing.validatePassword(userInput.getPassword(),
						userDetailsByEmail.getHashedPassword());
				if (verifyPassword) {
					String finalResponse = "{\"Result\":\"OK\"}";
					return finalResponse;
				} else {
					LOGGER.error("User has entered the incorrect password ");
					throw new KoritaException(ErrorStrings.invalidUserNamePassword);
				}
			}
		} catch (KoritaException e) {
			LOGGER.error("There is Korita Error during Login process in UserService", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + e.getLocalizedMessage() + "\"}";
			return error;
		} catch (Exception e) {
			LOGGER.error("There is some exception during Login process in UserService", e);
			String error = "{\"Result\":\"ERROR\",\"Message\":\"" + ErrorStrings.errorTech + "\"}";
			return error;
		}
	}

	
}

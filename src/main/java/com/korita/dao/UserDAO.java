
package com.korita.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.korita.bean.UserBean;
import com.korita.exception.KoritaException;
import com.korita.variables.SQLQueries;

/**
 * @author Ashok
 *
 */
@Repository("useDAO")
public class UserDAO {
	private static final Logger LOGGER = LogManager.getLogger(UserDAO.class.getName());
	
	@Autowired
	@Qualifier("dbDataSource")
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	/**
	 * Create a new user in database
	 * @param userInput
	 * @return int which can have value as 0 or 1,
	 * @throws KoritaException
	 */
	public int signUpUser(UserBean userInput) throws KoritaException {
		LOGGER.info("A new user will be created in database for email : "+userInput.getEmail());
		Connection connection =null;
		LOGGER.debug("Database connection object created");
		int userCreated = 0;
		try {
			connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQLQueries.createUser);
			preparedStatement.setString(1, userInput.getFirstName());
			preparedStatement.setString(2, userInput.getEmail());
			preparedStatement.setString(3, userInput.getHashedPassword());
			LOGGER.debug("User has been sucessfull created in the datebase");
			return preparedStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("There is a Database error while creating user with email : "+userInput.getEmail(),e);
			return userCreated;
			
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.error("There is a error while closing connection after creation of user with email : "+userInput.getEmail(),e);
				return userCreated;
			}
		}
		
	}
	
	
	/**
	 * Fetch the user details from the database using email id
	 * @param userInput
	 * @return user details 
	 * @throws KoritaException
	 */
	public UserBean findUserByEmail(UserBean userInput) throws KoritaException {
		LOGGER.info("User info will be fetched from database for email : "+userInput.getEmail());
		Connection connection = null;
		UserBean userBean = new UserBean();
		try {
			connection = dataSource.getConnection();
			LOGGER.debug("Database connection object created");
			PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.findUserbyEMail);
			preparedStatement.setString(1, userInput.getEmail());
			
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				LOGGER.debug("User info has been found in database");
				userBean = new UserBean(rs.getString("email"), rs.getString("hashedPassword"));
			}
			rs.close();
			preparedStatement.close();
			return userBean;
		} catch (SQLException e) {
			LOGGER.error("There is database error while checking user info in database",e);
			return userBean;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.error("There is error while closing connection after user info fetched from database",e);
					return userBean;
				}
			}
		}

	}

	
}

package com.korita.test;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.Statement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.korita.bean.UserBean;
import com.korita.dao.UserDAO;
import com.korita.utility.DBConnection;

public class KoritaDAOTest {

	/**
	 * will execute once before any of the test methods in this class.
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Class.forName("org.hsqldb.jdbc.JDBCDriver");
		try (Connection connection = DBConnection.getHSQLConnection(); Statement statement = connection.createStatement();) {
			statement.execute("CREATE TABLE myuser (firstname VARCHAR(45), email VARCHAR(45) NOT NULL UNIQUE, hashedpassword VARCHAR(255))");
			connection.commit();
		}
	}

	/**
	 * will execute once after all of the test methods are executed in this class.
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		try (Connection connection = DBConnection.getHSQLConnection(); Statement statement = connection.createStatement();) {
			statement.executeUpdate("DROP TABLE myuser");
			connection.commit();
		}
	}

	/**
	 * will execute before each test method in this class is executed.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * will execute after each test method in this class is executed.
	 * 
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void signUpUserDAOTest() {
		int inserted =0;
		UserBean userBean = new UserBean();
		userBean.setEmail("ab.bc");
		userBean.setPassword("123");
		UserDAO userDAO = new UserDAO();
		try {
			inserted = userDAO.signUpUser(userBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(1, inserted);
	}
	
	@Test
	public void signUpExistingUserDAOTest() {
		int inserted =0;
		UserBean userBean = new UserBean();
		userBean.setEmail("ab.bc");
		userBean.setPassword("123");
		UserDAO userDAO = new UserDAO();
		try {
			inserted = userDAO.signUpUser(userBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(0, inserted);
	}
	
	@Test
	public void findUserByEmailTest() {
		UserBean userBean = new UserBean();
		UserBean userBeanResult = null;
		userBean.setEmail("ab.bc");
		
		UserDAO userDAO = new UserDAO();
		try {
			userBeanResult =	userDAO.findUserByEmail(userBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(userBean.getEmail(), userBeanResult.getEmail());
	}
	
	@Test
	public void findUserByNotRegisteredEmailTest() {
		UserBean userBean = new UserBean();
		UserBean userBeanResult = null;
		userBean.setEmail("abc.bc");
		
		UserDAO userDAO = new UserDAO();
		try {
			userBeanResult =	userDAO.findUserByEmail(userBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(true, userBeanResult.getEmail()==null);
	}

}
